#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "elfutil.h"

const char* ESC_RED = "\033[0;31m";
const char* ESC_NC = "\033[0m";
const char* ESC_GREEN = "\033[0;32m";
const char* ESC_BLUE = "\033[0;34m";

const char* loadfile(const char* path, size_t* size)
{
    FILE* fp = fopen(path, "rb");

    if(fp == NULL)
    {
        printf("error: could not open '%s'", path);
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    *size = ftell(fp);

    rewind(fp);

    const char* buffer = (const char*)malloc(*size);

    fread((void*)buffer, *size, 1, fp);
    fclose(fp);

    return buffer;
}

int main(int argc, const char* argv[])
{
    const char *bin_path, *elf_path;
    uint32_t header_size, address, bin_address;
    size_t bin_size, elf_size;

    const char* bin_buffer;
    const char* elf_buffer;

    CElfContext* elf_context;
    CElfSection* elf_text_section;

    uint32_t elf_text_section_size;
    const char* elf_text_section_buffer;

    if(argc < 5)
    {
        printf("elfcmp <bin_path> <elf_path> <header_size> <address>\n");
        return EXIT_FAILURE;
    }

    bin_path = argv[1];
    elf_path = argv[2];

    header_size = strtol(argv[3], NULL, 0);
    address = strtol(argv[4], NULL, 0);

    bin_address = address - header_size;

    bin_buffer = loadfile(bin_path, &bin_size);

    if(bin_buffer == NULL)
    {
        return EXIT_FAILURE;
    }

    elf_buffer = loadfile(elf_path, &elf_size);

    if(elf_buffer == NULL)
    {
        return EXIT_FAILURE;
    }

    elf_context = new CElfContext(elf_buffer, elf_size);
    elf_text_section = elf_context->Section(".text");

    if(elf_text_section == NULL)
    {
        printf("error: elf object has no .text section\n");
        delete elf_context;
        return EXIT_FAILURE;
    }

    elf_text_section_size = elf_text_section->Size();
    elf_text_section_buffer = elf_text_section->Data(elf_context);



    printf("bin_addr  bin      elf\n");

    int num_mismatches = 0;

    for(uint32_t i = 0; i < elf_text_section_size; i += sizeof(uint32_t))
    {
        if(bin_address + i >= bin_size)
        {
            break;
        }

        uint32_t bin_opcode = bswap32(*(uint32_t*)&bin_buffer[bin_address + i]);
        uint32_t elf_opcode = bswap32(*(uint32_t*)&elf_text_section_buffer[i]);

        const char* color;
        int haveRelocation = 0;
        CElfSymbol* relocationSymbol = NULL;
        
        for(int j = 0; j < elf_context->NumTextRelocations(); j++)
        {
            CElfRelocation* rel = elf_context->TextRelocation(j);
            if(i == rel->Offset())
            {
                relocationSymbol = rel->Symbol(elf_context);
                haveRelocation = 1;
            }
        }

        if(bin_opcode != elf_opcode)
        {
            if(haveRelocation)
            {
                if((bin_opcode >> 26) == (elf_opcode >> 26))
                {
                    color = ESC_GREEN;
                }
                else
                {
                    color = ESC_RED;
                }
            }
            else
            {
                color = ESC_RED;
                num_mismatches++;
            }
        }
        else
        {
            color = ESC_GREEN;
        }

        for(int j = 0; j < elf_context->NumSymbols(); j++)
        {
            CElfSymbol* symbol = elf_context->Symbol(j);

            if(symbol->Value() == i &&
               symbol->Binding() == STB_GLOBAL &&
               symbol->Type() == STT_FUNC &&
               symbol->Size() != 0)
            {
                if(i > 0)
                {
                    printf("\n");
                }
                printf("%s (%08X):\n", symbol->Name(elf_context), address + i);
            }
        }

        printf("%08X: %s%08X %08X%s %s\n", bin_address + i, color, bin_opcode, elf_opcode, ESC_NC, haveRelocation?relocationSymbol->Name(elf_context):"");



        if(num_mismatches >= 150)
        {
            break;
        }
    }

    delete elf_context;
    return EXIT_SUCCESS;
}
