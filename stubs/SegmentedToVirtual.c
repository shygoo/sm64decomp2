typedef unsigned int u32;

extern u32 g_segments_table[32];

void* SegmentedToVirtual(u32 segoffset)
{
	u32 segment = segoffset >> 24;
	u32 offset = segoffset & 0x00FFFFFF;
	return (void*)(0x80000000 | (g_segments_table[segment] + offset));
}


// 80277FA8
u32 VirtualToSegmented(u32 segment, void* vaddr)
{
	return (segment << 24) + ((vaddr & 0x1FFFFFFF) - g_segments_table[segment]);
}
