#include "behavior_script.h"

const bhv_func_t g_behavior_script_table[] = {
    bhv_22, bhv_35, bhv_21, bhv_1b,
    bhv_1c, bhv_2c, bhv_29, bhv_1d,
    bhv_0a, bhv_0b, bhv_02, bhv_03,
    bhv_01, bhv_25, bhv_04, bhv_26,
    bhv_05, bhv_06, bhv_07, bhv_08,
    bhv_09, bhv_0c, bhv_0e, bhv_10,
    bhv_36, bhv_14, bhv_15, bhv_13,
    bhv_16, bhv_17, bhv_0d, bhv_0f,
    bhv_11, bhv_12, bhv_27, bhv_28,
    bhv_1e, bhv_18, bhv_1A, bhv_19
};