// 030008A0 jump while holding bobomb
// 830008AB throw bobomb in air
// land with bobomb 00000474

// 80251E24: mario.h/mario.c ?
int mario_is_collision_something(mario_t* mario) // 256
{
	int result = 0;
	
	if(_80251B54(mario, 0) != 0)
	{
		return result;
	}
	
	float min_normal;
	
	switch(_8025177C(mario)) // test collision surface maybe?
	{
		case 0x13:
			min_normal = g_unk_float0; // 3F7746EA 0.9659258127212524
			break;
		case 0x14:
			min_normal = g_unk_float1; // 3F708FB2 0.9396926164627075
			break;
		case 0x15:
			min_normal = g_unk_float3; // 3F5DB3D7 0.8660253882408142
			break;
		default:
			min_normal = g_unk_float2; // 3F5DB3D7 0.8660253882408142
			break;
	}
	
	result = 0;
	
	if(mario->cur_collision_face->normal_y < _2c)
	{
		result = 1;
	}
	
	return t8;
}

// 80252BD4:
int mario_action_yspd32(mario_t* mario, u32 action, u32 unknown) // (leaf) 68
{
	// set sp+8 = unknown here
	
	if(action == 0x000044F8 || action == 0x000044F9)
	{
		mario->y_speed = 32.0f;
	}
	
	return action;
}

// 80252CF4: DCF4
int mario_set_action(mario_t* mario, u32 action, u32 unknown) // 360
{
	u32 action_masked = action & 0x01C0;
	
	if(action_masked != 0x40)
	{
		switch(action_masked)
		{
		case 0x0080:
			_80252460(mario, action, unknown); // jump still
			break;
		case 0x00C0:
			mario_action_yspd32(mario, action, unknown); // sets yspd to 32 if action == 0x000044F8
			break;
		case 0x0100:
			_80252C18(mario, action, unknown);
			break;
		}
	}
	else
	{
		_802529E4(mario, action, unknown);
	}
	
	mario->flags &= 0xFFFCFFFF;
	
	if(mario->action & 0x0800 == 0)
	{
		mario->flags &= 0xFFFBFFFF;
	}
	
	mario->previous_action = mario->action;
	mario->action = action;
	mario->_1c = unknown;
	mario->_18 = 0;
	mario->_1a = 0;
	
	return 1;
}

// 80252E5C:
int mario_select_action_jump_running(mario_t* mario) // only jumps? 580
{
	if(11.0f < mario->_c0)
	{
		if(mario->_7c == 0)
		{
			mario_set_action(mario, 0x00000476, 0);
			return;
		}
		
		mario_set_action(mario, 0x00000477, 0); // holding?
		return;
	}
	
	if(mario_is_collision_something(mario))
	{
		_8025229C(mario);
	}
	else if(mario->_2b != 0 && mario->_b4 != 0) // b4 cap timer?
	{
		mario_set_action(mario, MARIO_ACTION_JUMP, 0);
	}
	else
	{
		switch(mario->previous_action)
		{
		case MARIO_ACTION_LAND: // land from jump
			mario_set_action(mario, MARIO_ACTION_DOUBLE_JUMP, 0);
			break;
			
		case MARIO_ACTION_LAND_2: // land from fall or jumpkick
			mario_set_action(mario, MARIO_ACTION_DOUBLE_JUMP, 0);
			break;
			
		case MARIO_ACTION_LAND_3: // land from double jump
			if(mario->flags & MARIO_FLAGS_WINGCAP)
			{
				mario_set_action(mario, MARIO_ACTION_JUMP_FLY, 0);
				break;
			}
			
			if(20.0f < mario->speed)
			{
				mario_set_action(mario, MARIO_ACTION_TRIPLE_JUMP, 0);
				break;
			}
			
			mario_set_action(mario, MARIO_ACTION_JUMP, 0);
			break;
		
		case 0x0C000233:
			mario_set_action(mario, MARIO_ACTION_DOUBLE_JUMP, 0);
			
		default:
			mario_set_action(mario, MARIO_ACTION_JUMP, 0);
			break;
		}
	}
	
	mario->_2b = 0;
	
	return 1;
}

// 802530A0:
void mario_force_action(mario_t* mario, u32 action, u32 unknown) // used for jump while standing still? 216
{
	// frame size is 0x20 but uses 20, 24, 28 for mario, action, unknown ?
	
	u32 unused = mario->_0c; // loaded to sp+1C, unused
	
	if(11.0f < mario->_c0)
	{
		if(mario->_7c == 0)
		{
			mario_set_action(mario, 0x00000476, 0);
			return;
			// broken branch here
		}
		
		mario_set_action(mario, 0x00000477, 0);
		return;
	}
	
	if(mario_is_collision_something(mario))
	{
		_8025229C(mario);
		return 1;
	}
	
	mario_set_action(mario, action, unknown);
	return 1;
	// broken branch here
}

//80255654
void mario_set_action_effect_rotation_and_speed(mario_t* mario, u32 action, u32 unknown) // 184
{
	s16 diff = mario->_74 - mario->rotation;
	
	if(!(diff < -16383) && (diff < 16384)) // -16383
	{
		mario->speed = 16.0f;
		mario->rotation = mario->_74;
	}
	else
	{
		mario->speed = -16.0f;
		mario->rotation = mario->_74 - 1; // addu
	}
	
	mario_set_action(mario, action, unknown);
}

// 802608B0:
int mario_select_action_standing(mario_t* mario) // all possible moves from standing state? 508
{
	_8024C6C0(mario);
	
	// test collision face against some shared num (always 0x3E95B1BE / 0.2923716902732849 ?)
	if(mario->cur_collision_face->normal_y < *(float*)0x80336970)
	{
		mario_set_action_effect_rotation_and_speed(mario, MARIO_ACTION_FALL, 0);
		return;
	}
	
	if(mario->_02 & 0x0400)
	{
		mario_set_action(mario, 0x00020226, 0);
		return;
	}
	
	if(mario->_02 & 0x0002)
	{
		mario_force_action(mario, MARIO_ACTION_JUMP, 0);
		return;
	}
	
	if(mario->_02 & 0x0004)
	{
		mario_set_action(mario, MARIO_ACTION_FALL, 0);
		return;
	}
	
	if(mario->_02 & 0x0008)
	{
		mario_set_action(mario, MARIO_ACTION_SLIDE, 0);
		return;
	}
	
	if(mario->_02 & 0x0010)
	{
		mario_set_action(mario, 0x0C000227, 0);
		return;
	}
	
	if(mario->_02 & 0x0001)
	{
		mario->rotation = mario->_24;
		mario_set_action(mario, MARIO_ACTION_RUN, 0);
		return;
	}
	
	if(mario->_02 & 0x2000)
	{
		mario_set_action(mario, MARIO_ACTION_PUNCH, 0);
		return;
	}
	
	if(mario->_02 & 0x4000)
	{
		mario_set_action(mario, MARIO_ACTION_DUCK_START, 0);
		return 0;
	}
}