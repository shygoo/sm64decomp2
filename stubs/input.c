#include "input.h"
#include "global.h"

#define SQUARE(_f) ((_f)*(_f))

// 80248304
void process_analog_input(ctrl_thing_t* ctrl) // 400 bytes
{
	ctrl->analog_x_fp = 0;
	ctrl->analog_y_fp = 0;
	
	if(ctrl->analog_x < -7)
	{
		ctrl->analog_x_fp = (float)(ctrl->analog_x + 6);
	}
	
	if(ctrl->analog_x >= 8)
	{
		ctrl->analog_x_fp = (float)(ctrl->analog_x - 6);
	}
	
	if(ctrl->analog_y < -7)
	{
		ctrl->analog_y_fp = (float)(ctrl->analog_y + 6);
	}
	
	if(ctrl->analog_y >= 8)
	{
		ctrl->analog_y_fp = (float)(ctrl->analog_y - 6);
	}
	
	// sqrtf @ 80323A50
	ctrl->analog_abs_fp = sqrtf(SQUARE(ctrl->analog_x_fp) + SQUARE(ctrl->analog_y_fp));
	
	if(64.0f < ctrl->analog_abs_fp)
	{
		ctrl->analog_x_fp = ctrl->analog_x_fp * (64.0f / ctrl->analog_abs_fp);
		ctrl->analog_y_fp = ctrl->analog_y_fp * (64.0f / ctrl->analog_abs_fp);
		ctrl->analog_abs_fp = 64.0f;
	}
}

// 80248498
void process_demo_command() // 416 bytes
{
	g_ctrl_main.c0.ptr_b->buttons &= 0xFF3F; // ignore L & R ?
	
	if(g_cur_demo_cmd == NULL)
	{
		return;
	}
	
	if(g_ctrl_main.c1.ptr_b != NULL)
	{
		// ?????
		g_ctrl_main.c1.ptr_b->analog_x = 0;
		g_ctrl_main.c1.ptr_b->analog_y = 0;
		g_ctrl_main.c1.ptr_b->buttons = 0;
	}
	
	// 80248508
	if(g_cur_demo_cmd->num_frames == 0)
	{
		g_ctrl_main.c0.ptr_b->analog_x = 0;
		g_ctrl_main.c0.ptr_b->analog_y = 0;
		g_ctrl_main.c0.ptr_b->buttons = 0x0080;
		return;
	}
	
	// 80248554
	u16 sp_06 = g_ctrl_main.c0.ptr_b->buttons & 0x1000;
	
	// 8024856C
	g_ctrl_main.c0.ptr_b->analog_x = g_cur_demo_cmd->analog_x;
	
	// 80248588
	g_ctrl_main.c0.ptr_b->analog_y = g_cur_demo_cmd->analog_y;
	
	// 802485A4
	g_ctrl_main.c0.ptr_b->buttons = ((g_cur_demo_cmd->buttons & 0xF0) << 8) + (g_cur_demo_cmd->buttons & 0x0F);
	
	// 802485d0
	g_ctrl_main.c0.ptr_b->buttons = g_ctrl_main.c0.ptr_b->buttons | sp_06;
	
	// 802485EC
	g_cur_demo_cmd->num_frames--;
	
	// 80248604
	if(g_cur_demo_cmd->num_frames == 0)
	{
		g_cur_demo_cmd += 4;
	}
}