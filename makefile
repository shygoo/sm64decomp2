IRIX_ROOT=/irixsys
CC511=$(IRIX_ROOT)/qemu-mips -L $(IRIX_ROOT)/411 $(IRIX_ROOT)/411/usr/bin/cc
CC53=$(IRIX_ROOT)/qemu-mips -L $(IRIX_ROOT)/53 $(IRIX_ROOT)/53/usr/bin/cc

export PATH := $(PATH):/n64chain/bin:/n64chain/x86_64-pc-linux-gnu/mips64-elf/lib

CC=$(CC53)
CFLAGS=-fullwarn -mips2 -g2 -non_shared -G0 -Xcpluscomm

SDIR=src
ODIR=obj

SFILES=$(wildcard $(SDIR)/*.c)
HFILES=$(wildcard $(SDIR)/*.h)
OFILES=$(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(SFILES))

.PHONY: recomp
recomp: $(OFILES)

$(ODIR)/%.o: $(SDIR)/%.c | $(ODIR)
	$(CC) $(CFLAGS) -c $^ -o $@

$(ODIR):
	mkdir $@

.PHONY: view
view: recomp
	mips64-elf-objdump -d obj/behavior_script.o

# -Mno-aliases

.PHONY: cmp
cmp: recomp
	./tools/elfcmp sm64.j.z64 ./obj/behavior_script.o 0x802845F0 0x80383B70

.PHONY: clean
clean:
	rm -rf obj
