#ifndef COLLISION_H
#define COLLISION_H

typedef struct
{
    u16 collision_type;   /* check collision.txt */
    u16 _0x02;
    u8 flag;
    u8 _0x05;
    s16 ymin;
    s16 ymax;
    s16 vertex1_x, vertex1_y, vertex1_z;   /* 0x0a */
    s16 vertex2_x, vertex2_y, vertex2_z;   /* 0x10 */
    s16 vertex3_x, vertex3_y, vertex3_z;   /* 0x16 */
    float normal_x; /* 1c */
    float normal_y; /* 20 */
    float normal_z;
    float negdot;
    u32 _0x2c;   /* unused? */
} collision_face_t;

#endif /* COLLISION_H */
