#include "global.h"
#include "hud.h"
#include "mario.h"
#include "n64.h"

/* manages backend values for the HUD */

/*extern vec3f_t nullpos; // 0x803331F0 */

/* @ 0x8024B13C */
void hud_main(void) /* 596 */
{
	s8 power_hi; /* sp_1e */
	s16 power;

	if(g_hud_updates_disabled)
	{
		return;
	}
	
	power = g_mario->power;
	
	if(power > 0)
	{
		power_hi = power >> 8;
	}
	else
	{
		power_hi = 0;
	}
	
	if(g_hud_coins_enabled > 0)
	{
		g_hud_state |= HUD_COINS;
	}
	else
	{
		g_hud_state &= ~HUD_COINS;
	}
	
	/* 8024B1B8 */
	if(g_hud_num_coins < g_mario->coins)
	{
		if(g_num_frames & 1) /* do it every other frame */
		{
			u32 sound_to_play; /* sp_18 */
			
			if(g_mario->action & 0x6000) /* under water */
			{
				sound_to_play = 0x38128081;
			}
			else
			{
				sound_to_play = 0x38128081;
			}
			
			/* 8024B230 */
			g_hud_num_coins++; 
			
			/* xyz_triplet @ ent+0x54, playsound 0x8031EB00 */
			PlaySound(sound_to_play, &g_mario->levelObject->position);
		}
	}
	
	/* 8024B24C */
	if(g_mario->lives >= 101)
	{
		g_mario->lives = 100;
	}
	
	/* 8024B274 */
	if(g_mario->coins >= 1000)
	{
		g_mario->coins = 999;
	}
	
	/* 8024B29C */
	if(g_hud_num_coins >= 1000)
	{
		g_hud_num_coins = 999;
	}
	
	/* 8024B2BC */
	g_hud_num_stars = g_mario->stars;
	
	/* 8024B2D0 */
	g_hud_num_lives = g_mario->lives;
	
	/* 8024B2E4 */
	g_hud_num_keys = g_mario->keys;
	
	/* 8024B2F8 */
	if(g_hud_power < power_hi)
	{
		/* 803331F0 = float triplet of 0,0,0 */
		PlaySound(0x700D0081, (vec3f_t*)0x803331F0);
	}
	
	/* 8024B324 */
	g_hud_power = power_hi;
	
	/* 8024B330 */
	if(g_mario->time_power_drain > 0)
	{
		/* keeps the meter in the middle of the screen */
		g_hud_state |= HUD_POWER_CHANGING;
	}
	else
	{
		g_hud_state &= ~HUD_POWER_CHANGING;
	}
}

/* 8024b3e4 */
