#include "global.h"
#include "entity.h"
#include "n64.h"
#include "behavior_script.h"

extern u32 D_8032C694; // used by beh_command_34

/* functions in level_functions.s */

// used by beh_command_37
extern void func_8029E388(LevelObject*, u32);
// ran at the start of a script if objFlags & OBJ_UNKNOWN_2000
extern u32 func_8029E694(LevelObject* object1, LevelObject* object2);
// calculates the distance between two LevelObjects
extern f32 object_calc_distance(LevelObject* object1, LevelObject* object2); // 8029DB7C
// used by beh_command_1c, beh_command_2c, beh_command_29
extern LevelObject* func_8029E964(LevelObject* object, u32 a, u32 arg0, u32 arg1);
extern void func_8029ECB4(f32);
// ran at end of script if objFlags & OBJ_UNKNOWN_0010
extern void func_8029F8EC(LevelObject*); 
// used by beh_command_00
extern s32 func_802A0D80(void*); // 802a0d80 
// ran at end of script if objFlags & OBJ_UNKNOWN_0002
extern void func_802A120C(void); 
// ran at end of script if objFlags & flagsLo & OBJ_UNKNOWN_0004
extern void func_802A12A4(void); 
// ran at end of script if objFlags & OBJ_UNKNOWN_0200
extern void func_802A2A84(LevelObject*); 
// ran at end of script if objFlags & OBJ_UNKNOWN_0800
extern void func_802A2A18(LevelObject*); 
// used by beh_command_00
extern void beh_common_init(void); // 802A3978
// ran at end of script if gCurrentObject->_1a0 != 0xFFFFFFFF
extern void func_802A4210(void);


// referenced by beh_command_00
extern u32 behScript13004FB4[]; // haunted chair
extern u32 behScript13005004[]; // mad piano
extern u32 behScript130032C0[]; // message panel

extern void func_8037C658(LevelObject*, void*); // used by beh_command_28, animation related

extern f32 func_80381794(float a, float b, float c);

extern u16 gRandomSeed; // 8038EEE0

void func_80383B70(u32 segptr)
{
	gBehCommandPtr = SegmentedToVirtual(segptr); // 802779A0
	gCurrentObject->stackIndex = 0;
}

u16 random_u16(void) // 80383BB0
{
	u16 temp1, temp2;

	if(gRandomSeed == 22026)
	{
		gRandomSeed = 0;
	}

	temp1 = (gRandomSeed & 0xFF) << 8;
	temp1 = temp1 ^ gRandomSeed;

	gRandomSeed = ((temp1 & 0x00FF) << 8) + ((temp1 & 0xFF00) >> 8);

	temp1 = ((temp1 & 0xFF) << 1) ^ gRandomSeed;
	temp2 = (temp1 >> 1) ^ 0xFF80;

	if((temp1 & 1) == 0)
	{
		if(temp2 == 43605)
		{
			gRandomSeed = 0;
		}
		else
		{
			gRandomSeed = temp2 ^ 0x1ff4;
		}
	}
	else
	{
		gRandomSeed = temp2 ^ 0x8180;
	}

	return gRandomSeed;
}

f32 random_f32(void)
{
	f32 rnd = random_u16();
	return rnd / (double)0x10000;
}

s32 random_sign(void)
{
	if(random_u16() >= 0x7FFF)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

void func_80383D68(LevelObject* object)
{
	// called after script execution if objFlags & 0x0001

	object->positionCopy.x = object->position.x;
	object->positionCopy.y = object->_dc + object->position.y;
	object->positionCopy.z = object->position.z;

	object->_1a = object->rotation2.x & 0xFFFF;
	object->_1c = object->rotation2.y & 0xFFFF;
	object->_1e = object->rotation2.z & 0xFFFF;
}

void cur_object_stack_push(u32 value)
{
	gCurrentObject->stack[gCurrentObject->stackIndex] = value;
	gCurrentObject->stackIndex++;
}

u32 cur_object_stack_pop(void)
{
	u32 value;
	gCurrentObject->stackIndex--;
	value = gCurrentObject->stack[gCurrentObject->stackIndex];
	return value;
}

void _beh_loop_forever(void) // ?
{
	for(;;);
}

s32 beh_command_22(void)
{
	cur_object_graph_x10();
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_35(void)
{
	gCurrentObject->graphFlags &= ~1; // TODO magic number
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_21(void)
{
	gCurrentObject->graphFlags |= OBJ_GRAPH_BILLBOARD;
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_1b(void)
{
	s32 index = (s16)(gBehCommandPtr[0] & 0xFFFF);
	gCurrentObject->geoLayoutPtr = g_geo_thing_ptr[index];
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_1c(void)
{
	u32 arg0 = gBehCommandPtr[1];
	u32 arg1 = gBehCommandPtr[2];
	
	LevelObject* object = func_8029E964(gCurrentObject, 0, arg0, arg1);
	
	object_copy_pos_rot(object, gCurrentObject);
	
	gBehCommandPtr += 3;
	return BEH_CONTINUE;
}

s32 beh_command_2c(void)
{
	u32 arg0 = gBehCommandPtr[1];
	u32 arg1 = gBehCommandPtr[2];
	
	LevelObject* object = func_8029E964(gCurrentObject, 0, arg0, arg1);
	
	object_copy_pos_rot(object, gCurrentObject);
	
	gCurrentObject->_6c = object;
	
	gBehCommandPtr += 3;
	return BEH_CONTINUE;
}

s32 beh_command_29(void)
{
	u32 behavParam = (s16)(gBehCommandPtr[0] & 0xFFFF);
	u32 arg1 = gBehCommandPtr[1];
	u32 arg2 = gBehCommandPtr[2];
	
	LevelObject* object = func_8029E964(gCurrentObject, 0, arg1, arg2);
	
	object_copy_pos_rot(object, gCurrentObject);
	
	object->behavParam = behavParam;
	
	gBehCommandPtr += 3;
	return BEH_CONTINUE;
}

s32 beh_command_1d(void)
{
	gCurrentObject->active = 0;
	return BEH_BREAK;
}

s32 beh_command_0a(void)
{
	return BEH_BREAK;
}

s32 beh_command_0b(void)
{
	return BEH_BREAK;
}

s32 beh_command_02(void)
{
	u32* jumpAddress;

	gBehCommandPtr++;
	cur_object_stack_push((u32)&gBehCommandPtr[1]);
	jumpAddress = (u32*)SegmentedToVirtual(gBehCommandPtr[0]);
	gBehCommandPtr = jumpAddress;

	return BEH_CONTINUE;
}

s32 beh_command_03(void)
{
	gBehCommandPtr = (u32*)cur_object_stack_pop();
	return BEH_CONTINUE;
}

s32 beh_command_01(void)
{
	s16 arg0 = gBehCommandPtr[0] & 0xFFFF;

	if(gCurrentObject->_1f4 < arg0 - 1)
	{
		gCurrentObject->_1f4++;
	}
	else
	{
		gCurrentObject->_1f4 = 0;
		gBehCommandPtr++;
	}

	return BEH_BREAK;
}

s32 beh_command_25(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 arg0 = cur_object_get_s32(objectOffset);

	if(gCurrentObject->_1f4 < (arg0 - 1))
	{
		gCurrentObject->_1f4++;
	}
	else
	{
		gCurrentObject->_1f4 = 0;
		gBehCommandPtr++;
	}

	return BEH_BREAK;
}

s32 beh_command_04(void)
{
	gBehCommandPtr++;
	gBehCommandPtr = SegmentedToVirtual(gBehCommandPtr[0]);
	return BEH_CONTINUE;
}

s32 beh_command_26(void)
{
	s32 value = (u8)(gBehCommandPtr[0] >> 16) & 0xFF;
	
	cur_object_stack_push((u32)&gBehCommandPtr[1]);
	cur_object_stack_push(value);
	
	gBehCommandPtr++;
	
	return BEH_CONTINUE;
}

s32 beh_command_05(void)
{
	s32 value = (s16)(gBehCommandPtr[0] & 0xFFFF);
	
	cur_object_stack_push((u32)&gBehCommandPtr[1]);
	cur_object_stack_push(value);
	
	gBehCommandPtr++;
	
	return BEH_CONTINUE;
}

s32 beh_command_06(void)
{
	u32 count = cur_object_stack_pop();
	
	count--;

	if(count != 0)
	{
		gBehCommandPtr = (u32*)cur_object_stack_pop();
		cur_object_stack_push((u32)gBehCommandPtr);
		cur_object_stack_push(count);
	}
	else
	{
		cur_object_stack_pop();
		gBehCommandPtr++;
	}

	return BEH_BREAK;
}

s32 beh_command_07(void)
{
	u32 count = cur_object_stack_pop();
	
	count--;

	if(count != 0)
	{
		gBehCommandPtr = (u32*)cur_object_stack_pop();
		cur_object_stack_push((u32)gBehCommandPtr);
		cur_object_stack_push(count);
	}
	else
	{
		cur_object_stack_pop();
		gBehCommandPtr++;
	}

	return BEH_CONTINUE;
}

s32 beh_command_08(void)
{
	cur_object_stack_push((u32)&gBehCommandPtr[1]);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_09(void)
{
	gBehCommandPtr = (u32*)cur_object_stack_pop();
	cur_object_stack_push((u32)gBehCommandPtr);

	return BEH_BREAK;
}

s32 beh_command_0c(void)
{
	void (*behavior_proc)(void) = (void*)gBehCommandPtr[1];
	
	behavior_proc();
	
	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_0e(void)
{
    u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
    f32 value = (s16)(gBehCommandPtr[0] & 0xFFFF);

    cur_object_set_f32(objectOffset, value);

    gBehCommandPtr++;
    return BEH_CONTINUE;
}

s32 beh_command_10(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s16 value = gBehCommandPtr[0] & 0xFFFF;
	
	cur_object_set_s32(objectOffset, value);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_36(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	u32 value = (s16)(gBehCommandPtr[1] & 0xFFFF);
	
	cur_object_set_s32(objectOffset, value);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_14(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	f32 sp20 = (s16)(gBehCommandPtr[0] & 0xFFFF);
	f32 sp1c = (s16)(gBehCommandPtr[1] >> 16);

	cur_object_set_f32(objectOffset, (sp1c * random_f32()) + sp20);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_15(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 sp20 = (s16)(gBehCommandPtr[0] & 0xFFFF);
	s32 sp1c = (s16)(gBehCommandPtr[1] >> 16);

	cur_object_set_s32(objectOffset, (s32)((f32)sp1c * random_f32()) + sp20);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_13(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 sp20 = (s16)(gBehCommandPtr[0] & 0xFFFF);
	s32 sp1c = (s16)(gBehCommandPtr[1] >> 16);
	
	cur_object_set_s32(objectOffset, (random_u16() >> sp1c) + sp20);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_16(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	f32 sp20 = (s16)(gBehCommandPtr[0] & 0xFFFF);
	f32 sp1c = (s16)(gBehCommandPtr[1] >> 16);
	
	cur_object_set_f32(objectOffset, (cur_object_get_f32(objectOffset) + sp20) + (sp1c * random_f32()));

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_17(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 sp20 = (s16)(gBehCommandPtr[0] & 0xFFFF);
	s32 sp1c = (s16)(gBehCommandPtr[1] >> 16);
	s32 sp18 = random_u16();

	cur_object_set_s32(objectOffset,
		(cur_object_get_s32(objectOffset) + sp20) + (sp18 >> sp1c));

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_0d(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	f32 value = (s16)(gBehCommandPtr[0] & 0xFFFF);
	
	cur_object_add_f32(objectOffset, value);
	
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_0f(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s16 value = gBehCommandPtr[0] & 0xFFFF;

	cur_object_add_s32(objectOffset, value);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_11(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 value = (s16)(gBehCommandPtr[0] & 0xFFFF);
	value &= 0xFFFF;

	cur_object_or_s32(objectOffset, value);
	
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_12(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 value = (s16)(gBehCommandPtr[0] & 0xFFFF);
	value = (value & 0xFFFF) ^ 0xFFFF;
	
	cur_object_and_s32(objectOffset, value);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_27(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;

	cur_object_set_s32(objectOffset, gBehCommandPtr[1]);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_28(void)
{	
	s32 animIndex = (u8)((gBehCommandPtr[0] >> 16) & 0xFF);
	u32* animPtr = gCurrentObject->animPtr;

	func_8037C658(gCurrentObject, &animPtr[animIndex]);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_1e(void)
{
	f32 x = gCurrentObject->position.x;
	f32 y = gCurrentObject->position.y;
	f32 z = gCurrentObject->position.z;
	f32 sp18 = func_80381794(x, y + 200.0f, z);

	gCurrentObject->position.y = sp18;
	gCurrentObject->_ec |= 2;

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_18(void)
{
	/* no operation */
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_1A(void)
{
	/* no operation */
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_19(void)
{
	/* no operation */
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_1f(void)
{
	u32 objectOffsetDst = (u8)((gBehCommandPtr[0] >> 16) & 0xFF);
	u32 objectOffsetSrc1 = (u8)((gBehCommandPtr[0] >> 8) & 0xFF);
	u32 objectOffsetSrc2 = (u8)((gBehCommandPtr[0]) & 0xFF);

	cur_object_set_f32(objectOffsetDst,
		cur_object_get_f32(objectOffsetSrc1) + cur_object_get_f32(objectOffsetSrc2));

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_20(void)
{
	u32 objectOffsetDst = (u8)((gBehCommandPtr[0] >> 16) & 0xFF);
	u32 objectOffsetSrc1 = (u8)((gBehCommandPtr[0] >> 8) & 0xFF);
	u32 objectOffsetSrc2 = (u8)((gBehCommandPtr[0]) & 0xFF);

	cur_object_set_s32(objectOffsetDst,
		cur_object_get_s32(objectOffsetSrc1) + cur_object_get_s32(objectOffsetSrc2));

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_23(void)
{
	s16 colSphereX = gBehCommandPtr[1] >> 16;
	s16 colSphereY = gBehCommandPtr[1] & 0xFFFF;

	gCurrentObject->colSphereX = colSphereX;
	gCurrentObject->colSphereY = colSphereY;

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_2e(void)
{
	s16 arg0 = gBehCommandPtr[1] >> 16;
	s16 arg1 = gBehCommandPtr[1] & 0xFFFF;

	gCurrentObject->_200 = arg0;
	gCurrentObject->_204 = arg1;

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_2b(void)
{
	s16 colSphereX = gBehCommandPtr[1] >> 16;
	s16 colSphereY = gBehCommandPtr[1] & 0xFFFF;
	s16 unknown = gBehCommandPtr[2] >> 16;

	gCurrentObject->colSphereX = colSphereX;
	gCurrentObject->colSphereY = colSphereY;
	gCurrentObject->_208 = unknown;

	gBehCommandPtr += 3;
	return BEH_CONTINUE;
}

s32 beh_command_24(void)
{
	/* no operation */
	s16 arg0 = (u8)((gBehCommandPtr[0] >> 16) & 0xFF);
	s16 arg1 = gBehCommandPtr[0] & 0xFFFF;

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_00(void)
{
	if(func_802A0D80(behScript13004FB4))
	{
		beh_common_init();
	}

	if(func_802A0D80(behScript13005004))
	{
		beh_common_init();
	}

	if(func_802A0D80(behScript130032C0))
	{
		gCurrentObject->collisionDistance = 150.0f;
	}

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

void beh_unknown_8038556C(s32 lastIndex)
{
	s8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	u32 table[16];
	s32 i;

	for(i = 0; i <= lastIndex / 2; i += 2)
	{
		table[i] = (s16)(gBehCommandPtr[i + 1] >> 16);
		table[i + 1] = (s16)(gBehCommandPtr[i + 1] & 0xFFFF);
	}

	cur_object_set_s32(objectOffset, table[(s32)(lastIndex * random_f32())]);
}

s32 beh_command_2a(void)
{
	void* collisionPtr = SegmentedToVirtual(gBehCommandPtr[1]);
	gCurrentObject->collisionPtr = collisionPtr;
	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_2d(void)
{
	gCurrentObject->_164_x = gCurrentObject->position.x;
	gCurrentObject->_168_y = gCurrentObject->position.y;
	gCurrentObject->_16c_z = gCurrentObject->position.z;
	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_2f(void)
{
	gCurrentObject->interaction = gBehCommandPtr[1];

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_31(void)
{
	gCurrentObject->_190 = gBehCommandPtr[1];

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_32(void)
{
	u8 sp1f = (gBehCommandPtr[0] >> 16) & 0xFF; // unused
	s16 sp1c = gBehCommandPtr[0] & 0xFFFF;

	func_8029ECB4((f32)sp1c / 100.0f);

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

s32 beh_command_30(void)
{
	f32 sp04, sp00;

	gCurrentObject->_128 = (f32)(s16)(gBehCommandPtr[1] >> 16);
	gCurrentObject->_e4 = (f32)(s16)(gBehCommandPtr[1] & 0xFFFF) / 100.0f;
	gCurrentObject->_158 = (f32)(s16)(gBehCommandPtr[2] >> 16) / 100.0f;
	gCurrentObject->_12c = (f32)(s16)(gBehCommandPtr[2] & 0xFFFF) / 100.0f;
	gCurrentObject->_170 = (f32)(s16)(gBehCommandPtr[3] >> 16) / 100.0f;
	gCurrentObject->_174 = (f32)(s16)(gBehCommandPtr[3] & 0xFFFF) / 100.0f;

	// unused parameters
	sp04 = (f32)(s16)(gBehCommandPtr[4] >> 16) / 100.0f;
	sp00 = (f32)(s16)(gBehCommandPtr[4] & 0xFFFF) / 100.0f;

	gBehCommandPtr += 5;
	return BEH_CONTINUE;
}

s32 beh_command_33(void)
{
	u8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s32 flags = gBehCommandPtr[1];

	flags = flags ^ 0xFFFFFFFF;

	object_and_s32(gCurrentObject->nextObjectPtr2, objectOffset, flags);

	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_37(void)
{
	u32 arg0 = gBehCommandPtr[1];
	func_8029E388(gCurrentObject, arg0);
	gBehCommandPtr += 2;
	return BEH_CONTINUE;
}

s32 beh_command_34(void)
{
	s8 objectOffset = (gBehCommandPtr[0] >> 16) & 0xFF;
	s16 arg1 = (gBehCommandPtr[0] & 0xFFFF);

	if((D_8032C694 % arg1) == 0)
	{
		cur_object_add_s32(objectOffset, 1);
	}

	gBehCommandPtr++;
	return BEH_CONTINUE;
}

void _beh_stub(void)
{
	// (empty function)
}

void execute_behavior_script(void)
{
	u32 _unused;

	s16 flagsLo;
	f32 distanceFromMario;
	BehCommandProc next_beh_command_proc;
	s32 behProcResult;
	
	flagsLo = (s16)gCurrentObject->objFlags;

	if(flagsLo & OBJ_CALC_DISTANCE_FROM_MARIO)
	{
		gCurrentObject->distanceFromMario = object_calc_distance(gCurrentObject, gMarioObject);
		distanceFromMario = gCurrentObject->distanceFromMario;
	}
	else
	{
		distanceFromMario = 0.0f;
	}

	if(flagsLo & OBJ_UNKNOWN_2000)
	{
		gCurrentObject->_160 = func_8029E694(gCurrentObject, gMarioObject);
	}

	if(gCurrentObject->previousAction != gCurrentObject->action)
	{
		gCurrentObject->timer = 0,
		gCurrentObject->_150 = 0,
		gCurrentObject->previousAction = gCurrentObject->action;
	}

	gBehCommandPtr = gCurrentObject->behScriptPtr;

	do
	{
		next_beh_command_proc = gBehCommandProcs[gBehCommandPtr[0] >> 24];
		behProcResult = next_beh_command_proc();
	} while(behProcResult == BEH_CONTINUE);

	gCurrentObject->behScriptPtr = gBehCommandPtr;

	if(gCurrentObject->timer < 0x3FFFFFFF)
	{
		gCurrentObject->timer++;
	}

	if(gCurrentObject->previousAction != gCurrentObject->action) 
	{
		gCurrentObject->timer = 0,
		gCurrentObject->_150 = 0,
		gCurrentObject->previousAction = gCurrentObject->action;
	}

	flagsLo = gCurrentObject->objFlags;

	if(flagsLo & OBJ_UNKNOWN_0010)
	{
		func_8029F8EC(gCurrentObject);
	}

	if(flagsLo & OBJ_UNKNOWN_0008)
	{
		gCurrentObject->rotation2.y = gCurrentObject->rotation.y;
	}

	if(flagsLo & OBJ_UNKNOWN_0002)
	{
		func_802A120C();	
	}

	if(flagsLo & OBJ_UNKNOWN_0004)
	{
		func_802A12A4();
	}

	if(flagsLo & OBJ_UNKNOWN_0200)
	{
		func_802A2A84(gCurrentObject);
	}

	if(flagsLo & OBJ_UNKNOWN_0800)
	{
		func_802A2A18(gCurrentObject);
	}

	if(flagsLo & OBJ_UNKNOWN_0001)
	{
		func_80383D68(gCurrentObject);
	}

	if(gCurrentObject->_1a0 != 0xFFFFFFFF)
	{
		func_802A4210();
	}
	else if((flagsLo & OBJ_CALC_DISTANCE_FROM_MARIO) && gCurrentObject->collisionPtr == NULL)
	{
		if((flagsLo & OBJ_UNKNOWN_0080) == 0)
		{
			if(distanceFromMario > gCurrentObject->drawingDistance)
			{
				gCurrentObject->graphFlags &= 0xFFFFFFFE;
				gCurrentObject->active |= 2;
			}
			else if(gCurrentObject->_124 == 0)
			{
				gCurrentObject->graphFlags |= 1;
				gCurrentObject->active &= 0xFFFFFFFD;
			}
		}	
	}
}
