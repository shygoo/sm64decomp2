#ifndef BEHAVIOR_SCRIPT_H
#define BEHAVIOR_SCRIPT_H

#define BEH_BREAK    1
#define BEH_CONTINUE 0

/* flags for levelObject->objFlags */
#define OBJ_UNKNOWN_0001             0x0001
#define OBJ_UNKNOWN_0002             0x0002
#define OBJ_UNKNOWN_0004             0x0004
#define OBJ_UNKNOWN_0008             0x0008
#define OBJ_UNKNOWN_0010             0x0010
#define OBJ_UNKNOWN_0020             0x0020 // ?
#define OBJ_CALC_DISTANCE_FROM_MARIO 0x0040
#define OBJ_UNKNOWN_0080             0x0080
#define OBJ_UNKNOWN_0100             0x0100 // ?
#define OBJ_UNKNOWN_0200             0x0200
#define OBJ_UNKNOWN_0400             0x0400
#define OBJ_UNKNOWN_0800             0x0800
#define OBJ_UNKNOWN_1000             0x1000 // ?
#define OBJ_UNKNOWN_2000             0x2000
#define OBJ_UNKNOWN_4000             0x4000 // ?
#define OBJ_UNKNOWN_8000             0x8000

/*flags for levelObject->graphFlags */
#define OBJ_GRAPH_BILLBOARD 0x0004

#define BEH_OBJ_OFFSET_PUBLIC 0x88

#define cur_object_set_f32(offset, value) \
    *(f32*)((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC + (offset)*4) = (f32)(value)

#define cur_object_add_f32(offset, value) \
    *(f32*)((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC + (offset)*4) += (f32)(value)

#define cur_object_get_f32(offset) \
    (*(f32*)((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC + (offset)*4))

#define cur_object_set_s32(offset, value) \
    *(s32*)((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC + (offset)*4) = (s32)(value)

#define cur_object_get_s32(offset) \
    (*(s32*)(((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC) + (offset)*4))

#define object_and_s32(object, offset, value) \
    *(s32*)(((u8*)object + BEH_OBJ_OFFSET_PUBLIC) + (offset)*4) &= (s32)(value)

#define cur_object_and_s32(offset, value) \
    object_and_s32(gCurrentObject, offset, value)

#define cur_object_or_s32(offset, value) \
    *(s32*)(((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC) + (offset)*4) |= (s32)(value)

#define cur_object_add_s32(offset, value) \
    *(s32*)(((u8*)gCurrentObject + BEH_OBJ_OFFSET_PUBLIC) + (offset)*4) += (s32)(value)

typedef s32 (*BehCommandProc)(void);
extern BehCommandProc gBehCommandProcs[]; /* 8038B9B0 */

void func_80383B70(u32 segptr);
u16 random_u16(void); // 80383BB0
f32 random_f32(void); // 80383CB4
s32 random_sign(void);
void func_80383D68(LevelObject* object);
void cur_object_stack_push(u32 value);
u32 cur_object_stack_pop(void);

void _beh_loop_forever(void); // ?

s32 beh_command_22(void);
s32 beh_command_35(void);
s32 beh_command_21(void);
s32 beh_command_1b(void);
s32 beh_command_1c(void);
s32 beh_command_2c(void);
s32 beh_command_29(void);
s32 beh_command_1d(void);
s32 beh_command_0a(void);
s32 beh_command_0b(void);
s32 beh_command_02(void);
s32 beh_command_03(void);
s32 beh_command_01(void);
s32 beh_command_25(void);
s32 beh_command_04(void);
s32 beh_command_26(void);
s32 beh_command_05(void);
s32 beh_command_06(void);
s32 beh_command_07(void);
s32 beh_command_08(void);
s32 beh_command_09(void);
s32 beh_command_0c(void);
s32 beh_command_0e(void);
s32 beh_command_10(void);
s32 beh_command_36(void);
s32 beh_command_14(void);
s32 beh_command_15(void);
s32 beh_command_13(void);
s32 beh_command_16(void);
s32 beh_command_17(void);
s32 beh_command_0d(void);
s32 beh_command_0f(void);
s32 beh_command_11(void);
s32 beh_command_12(void);
s32 beh_command_27(void);
s32 beh_command_28(void);
s32 beh_command_1e(void);
s32 beh_command_18(void);
s32 beh_command_1A(void);
s32 beh_command_19(void);
s32 beh_command_1f(void);
s32 beh_command_20(void);
s32 beh_command_23(void);
s32 beh_command_2e(void);
s32 beh_command_2b(void);
s32 beh_command_24(void);
s32 beh_command_00(void);
void beh_unknown_8038556C(s32 a0);
s32 beh_command_2a(void);
s32 beh_command_2d(void);
s32 beh_command_2f(void);
s32 beh_command_31(void);
s32 beh_command_32(void);
s32 beh_command_30(void);
s32 beh_command_33(void);
s32 beh_command_37(void);
s32 beh_command_34(void);

void _beh_stub(void); // ?

void execute_behavior_script(void);

#endif /* BEHAVIOR_SCRIPT_H */
