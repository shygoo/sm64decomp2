#ifndef _N64_H
#define _N64_H

#define NULL ((void*)0)

typedef long long          s64;
typedef int                s32;
typedef short              s16;
typedef char                s8;
typedef unsigned long long u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char       u8;

typedef float  f32;
typedef double f64;

typedef struct
{
    float x, y, z;
} vec3f_t;

typedef struct
{
    s32 x, y, z;
} vec3_t;

#endif
