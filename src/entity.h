#ifndef ENTITY_H
#define ENTITY_H

#include "n64.h"

/* thanks frauber */

typedef struct LevelObject /* Regular objects, Mario also has its own struct like this */
{
    u16    graphNodeType; /* 0x00 */
    s16    graphFlags;
    struct LevelObject *previousLinkedObject; /* previous linked list object */
    struct LevelObject *nextLinkedObject;     /* next linked list object */ 
    u32    graphParent;
    u32    graphChild;   /* 0x10 */
    u32   *geoLayoutPtr; /* 0x14 */
    s16    _18;
    s16    _1a;
    s16    _1c;
    s16    _1e;
    vec3f_t positionCopy; /* 0x20 */
    f32     xScaling;     /* 0x2c */
    f32     yScaling;     /* 0x30 */
    f32     zScaling;     /* 0x34 */
    u16     _38;
    u16     _3a;
    u32     animation;            /* 0x3c - current animation */
    u16     animCurrentFrame;     /* 0x40 */
    u16     animTimer;            /* timer, animation related? */
    u16     animCurrentFrameCopy;
    u16     _46;
    u32     _48;
    u32     _4c;
    u32     matrixPtr;    /* 0x50 */
    f32     f32_54;
    f32     f32_58;
    f32     f32_5c;
    struct LevelObject *nextObjectPtr;  /* 0x60: re-check this */
    u32     _64;
    struct LevelObject *nextObjectPtr2; /* 0x68: re-check this (childObj) */
    struct LevelObject *_6c;
    u32     _70;                        /* 0x70 */
    s16     active;                     /* 74 0x0000 = inactive, 0x0101 = active */
    u16     _76;                        /* collision flag according to YE */
    struct LevelObject *collidedObjPtr; /* according to YE, pointer to object collided with */
    u32     _7c;
    u32     _80;
    u32     _84;
	u32     _88;
    u32     objFlags;
    u32     _90;
    u32     _94;
    u32     _98;
    u32     _9c;
    vec3f_t position;  /* 0xa0 */
    vec3f_t speed;     /* 0xac */
    f32     speed_2;
    u32     _bc;
    u32     _c0;
    vec3_t  rotation;  /* 0xc4 - rotation triplet */
    vec3_t  rotation2; // 0xd0
    f32     _dc;
    u32     _e0;
    f32     _e4; /* gravity related? ySpeed - 0xe4 ? */
    u32     _e8;
    u32     _ec;
    u32     _f0;
    u32     _f4; /* obj type for some behaviors (ie, ice bully), for AMPS, radius of rotation */
    u32     _f8;
    u32     _fc;
    u32     _100;
    u32     _104;
    u32     _108;
    u32     _10c;
    u32     _110;
    u32     _114;
    u32     _118;
    u32     _11c;
    u32    *animPtr; /* 0x120 = (set by 0x27 26 behavior command) entry for animation? */
    u32     _124; /* in some behaviors, action related? */
    f32  _128;
    f32  _12c;
    u32  interaction;            
    u32  _134;
    u32  _138;
    u32  _13c;
    u32  _140;
    u32  behavParam;
    u32  _148;
    u32  action;
    u32  _150;   /* 0x150 = also reset when action changes */
    s32  timer;  /* always incremented. When action changes, it's set to 0 */
    f32  _158;                 
    f32  distanceFromMario;
    u32  _160;
    f32  _164_x;
    f32  _168_y;
    f32  _16c_z;
    f32  _170;
    f32  _174;
    u32  _178;
    u32  transparency;
    u32  damageToMario;
    u32  health;               /* Health (ie, for King bob-omb and whomp */
    u32  behavParam2;          /* re-check */
    u32  previousAction;       /* used to reset the 0x154 timer */
    u32  _190;                 /* 0x190 */
    f32  collisionDistance;    /*  NOTE: if collisionDistance < disappearDistance then disappearDistance = collisionDistance */
    u32  _198;
    f32  drawingDistance;
    u32  _1a0;
    u32  _1a4;
    u32  _1a8;
    u32  _1ac;
    u32  _1b0;
    u32  _1b4;
    u32  _1b8;
    u32  _1bc;
    u32  _1c0;
    u32  _1c4;
    u32  _1c8;
    u32 *behScriptPtr;
    u32  stackIndex; /* 0x1d0 */
    u32  stack[8]; /* will have to check on this size*/
    s16  _1f4;
    u16  _1f6;
    f32  colSphereX; // 1f8
    f32  colSphereY; // 1fc
    f32  _200;
    f32  _204;
    f32  _208;
    u32  behScriptEntry;
    u32  _210;
    u32  collideObjPtr;  // 214
    void*  collisionPtr; /* set by behavior script (0x2A command) */
    u32  _21c;
    u32  _220;
    u32  _224;
    u32  _228;
    u32  _22c;
    u32  _230;
    u32  _234;
    u32  _238;
    u32  _23c;
    u32  _240;
    u32  _244;
    u32  _248;
    u32  _24c;
    u32  _250;
    u32  _254;
    u32  _258;
    u32  behavParamCopyPtr;
} LevelObject;

/* 0x130 
00 = Something Solid. Can't grab. Mario walks around, Can jump over.
01 = Crashed when jumping at it, Used by Hoot.
02 = Grabbing
04 = Going through door
08 = Knocks mario back and dissappears. No damage.
10 = Something Solid, Can't grab, Mario walks around, Can't jump over, Seems somewhat thin..
40 = Climbing 
*/

/* 0x214
pointer to another object (collision happening)?. 
Can be used to detect if Mario is on top of the object by comparing
value with Mario's pointer
*/

void object_copy_pos_rot(LevelObject* target, LevelObject* source);
void object_copy_position(LevelObject* target, LevelObject* source);
void object_copy_rotation(LevelObject* target, LevelObject* source);

void cur_object_graph_x10(void);

#endif /* ENTITY_H */
