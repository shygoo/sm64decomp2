#include "entity.h"
#include "global.h"


/* 8029F0E0 */
void object_copy_pos_rot(LevelObject* target, LevelObject* source)
{
	object_copy_position(target, source);
	object_copy_rotation(target, source);
}


/* 8029F120 */
void object_copy_position(LevelObject* target, LevelObject* source)
{
	target->position.x = source->position.x;
	target->position.y = source->position.y;
	target->position.z = source->position.z;
}

/* 8029F148 */
void object_copy_rotation(LevelObject* target, LevelObject* source)
{
    target->rotation.x = source->rotation.x;
    target->rotation.y = source->rotation.y;
    target->rotation.z = source->rotation.z;
	
    target->rotation2.x = source->rotation2.x;
    target->rotation2.y = source->rotation2.y;
    target->rotation2.z = source->rotation2.z;
}

/* 8029F6BC */
void cur_object_graph_x10(void)
{
	gCurrentObject->graphFlags |= 0x10;
}
